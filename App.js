import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import {Platform} from 'react-native';
import { Text, View, Button, Vibration } from 'react-native';
import {KeyboardAvoidingView} from 'react-native';
import KeyboardSpacer from "react-native-keyboard-spacer";

// const baseUrl = 'http://192.168.0.103/api/v1/expo-token';
const baseUrl = 'http://panaroma.technodev.xyz/api/v1/expo-token';

export default class MyWeb extends Component {
    state = {
        expoPushToken : '',
        notification: {},
    };

    registerForPushNotificationsAsync= async()=> {
        const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
        let finalStatus = existingStatus;

        // only ask if permissions have not already been determined, because
        // iOS won't necessarily prompt the user a second time.
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }

        // Stop here if the user did not grant permissions
        if (finalStatus !== 'granted') {
            return;
        }
        console.log(finalStatus)

        // Get the token that uniquely identifies this device
        console.log("Notification Token: ", await Notifications.getExpoPushTokenAsync());
        let token = await Notifications.getExpoPushTokenAsync();
        // this.setState({expoPushToken: token});
        // console.log(token);

        let formdata = new FormData();
        formdata.append("expo_token", token)
        fetch(baseUrl, {method: "POST", body: formdata})
            .then((response) => response.json())
            .then((responseData) => {
                console.log(responseData)
            })
            .done();
    }

    async componentDidMount() {
        await this.registerForPushNotificationsAsync();

    }

    render() {
        return (
            // <KeyboardAvoidingView
            //     style={{ flex: 1 }}
            //     behavior='padding'
            //     enabled={Platform.OS === 'android'}
            // >
            //     <WebView
            //         source={{uri: 'http://panaroma.technodev.xyz/customer/'}}
            //         // source={{uri: 'http://192.168.0.103/customer'}}
            //     />
            // </KeyboardAvoidingView>

                <View style={{ flex: 1, marginTop: 30 }}>
                    {console.log('hi ' + this.state.expoPushToken)}
                    <WebView
                        source={{uri: 'http://panaroma.technodev.xyz/customer/'}}
                        // source={{uri: 'http://192.168.0.103/customer'}}
                    />
                    <KeyboardSpacer/>
                </View>
        )
    }
}
